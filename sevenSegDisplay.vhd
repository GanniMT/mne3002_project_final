--------------------------------------------------------------------------------
-- Project		: Seven Segment Display Driver
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 04/01/2022
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: sevenSegDisplay.vhd
-- Design		: Interfacing a PS2 Keyboard with a Matrix Display on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity sevenSegDisp is
port (clk				: in std_logic;
	  hexIn				: in std_logic_vector(7 downto 0);
	  anodeSel 			: out std_logic_vector(3 downto 0);
	  sevenSegDispOut	: out std_logic_vector(6 downto 0));
end entity sevenSegDisp;

architecture rtl of sevenSegDisp is

signal fourBitBCD	: std_logic_vector(3 downto 0);
signal clkCounter	: natural range 0 to 50000000	:= 0;
signal anodeNo		: natural range 0 to 3			:= 0;

begin

-- Counter
process(clk)
begin
	if rising_edge(clk) then
		clkCounter <= clkCounter + 1;
		if clkCounter >= 100000 then
			clkCounter <= 0;
			anodeNo <= anodeNo + 1;
			if anodeNo > 3 then
				anodeNo <= 0;
			end if;
		end if;
	end if;
end process;

-- Deciding which anode to enable
process(anodeNo)
begin
	case (anodeNo) is
		when 0 => --AN3
            anodeSel <= "1110";
            fourBitBCD   <= hexIn(3 downto 0);

        when 1 => --AN2
            anodeSel <= "1101";
            fourBitBCD   <= hexIn(7 downto 4);

        when 2 => --AN1
            anodeSel <= "1011";
            fourBitBCD   <= "0000";

        when 3 => --AN0
            anodeSel <= "0111";
            fourBitBCD   <= "0000";

        when others =>
            null;
	end case;
end process;

-- Translating BCD value to Seven Segment Display
process(fourBitBCD)
begin
	case (fourBitBCD) is
        when "0000" =>        --ABCDEFG 
            sevenSegDispOut <= "0000001"; -- "0" 

        when "0001" =>
            sevenSegDispOut <= "1001111"; -- "1"

        when "0010" =>
            sevenSegDispOut <= "0010010"; -- "2"

        when "0011" =>
            sevenSegDispOut <= "0000110"; -- "3"

        when "0100" =>
            sevenSegDispOut <= "1001100"; -- "4"

        when "0101" =>
            sevenSegDispOut <= "0100100"; -- "5"

        when "0110" =>
            sevenSegDispOut <= "0100000"; -- "6"

        when "0111" =>
            sevenSegDispOut <= "0001111"; -- "7"

        when "1000" =>
            sevenSegDispOut <= "0000000"; -- "8"

        when "1001" =>
            sevenSegDispOut <= "0000100"; -- "9"

        when "1010" =>
            sevenSegDispOut <= "0001000"; -- A

        when "1011" =>
            sevenSegDispOut <= "1100000"; -- B

        when "1100" =>
            sevenSegDispOut <= "0110001"; -- C

        when "1101" =>
            sevenSegDispOut <= "1000010"; -- D

        when "1110" =>
            sevenSegDispOut <= "0110000"; -- E

        when "1111" =>
            sevenSegDispOut <= "0111000"; -- F

        when others =>
            sevenSegDispOut <= "1111111"; -- Display nothing  
	end case;
end process;
	
end architecture rtl;
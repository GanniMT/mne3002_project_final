--------------------------------------------------------------------------------
-- Project		: Dot Matrix Display Driver
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 05/02/2022
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: dotMatDisplay.vhd
-- Design		: Interfacing a PS2 Keyboard with a Matrix Display on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dotMatrix is
port (clk			: in std_logic;
	  rowPattern0	: in std_logic_vector(4 downto 0);
	  rowPattern1	: in std_logic_vector(4 downto 0);
	  rowPattern2	: in std_logic_vector(4 downto 0);
	  rowPattern3	: in std_logic_vector(4 downto 0);
	  rowPattern4	: in std_logic_vector(4 downto 0);
	  rowPattern5	: in std_logic_vector(4 downto 0);
	  rowPattern6	: in std_logic_vector(4 downto 0);
	  matrixRow		: out std_logic_vector(6 downto 0);
	  matrixCol		: out std_logic_vector(4 downto 0));
end entity dotMatrix;

architecture rtl of dotMatrix is

-- Signals related to the Counter
signal clkCounter		: unsigned(15 downto 0) := (others => '0');
signal nextClkCounter	: unsigned(15 downto 0);
signal clkCounterMSB	: std_logic := '0';

-- Pattern Registers for a Given Row
signal rowPattern		: std_logic_vector(4 downto 0) := (others => '0');
signal nextRowPattern	: std_logic_vector(4 downto 0);

-- Signals related to Row Refreshing
signal rowCounter		: unsigned(2 downto 0) := (others => '0');
signal nextRowCounter	: unsigned(2 downto 0);

-- Row enables for combined rows
signal rowEnables		: std_logic_vector(6 downto 0) := (others => '1');
signal nextRowEnables	: std_logic_vector(6 downto 0);

-- Row enables
signal nextRowEnable	: std_logic;
signal row0Enable		: std_logic;
signal row1Enable		: std_logic;
signal row2Enable		: std_logic;
signal row3Enable		: std_logic;
signal row4Enable		: std_logic;
signal row5Enable		: std_logic;
signal row6Enable		: std_logic;

begin

-- Continous Counting
nextClkCounter <= clkCounter + 1;
nextRowCounter <= rowCounter + 1;

-- Driving the new clk count value into register
-- and obtaining MSB of register
process(clk)
begin
	if rising_edge(clk) then
		clkCounter <= nextClkCounter;
		clkCounterMSB <= clkCounter(15);
	end if;
end process;

nextRowEnable <= clkCounter(15) and (not clkCounterMSB);

matrixRow <= rowEnables;
matrixCol <= rowPattern;

-- Turning off all rows and turning on specified row and loads
-- intended pattern to display in register
process(rowCounter, rowPattern, row0Enable,
		row1Enable, row2Enable, row3Enable,
		row4Enable, row5Enable, row6Enable)
begin
	-- '1' used because Dot Matrix Display
	-- makes use of Common Cathode
	row0Enable <= '1';
	row1Enable <= '1';
	row2Enable <= '1';
	row3Enable <= '1';
	row4Enable <= '1';
	row5Enable <= '1';
	row6Enable <= '1';

	case (rowCounter) is
		when "001" =>
			nextRowPattern	<= rowPattern0;
			row0Enable		<= '0';

		when "010" =>
			nextRowPattern	<= rowPattern1;
			row1Enable		<= '0';

		when "011" =>
			nextRowPattern	<= rowPattern2;
			row2Enable		<= '0';

		when "100" =>
			nextRowPattern	<= rowPattern3;
			row3Enable		<= '0';

		when "101" =>
			nextRowPattern	<= rowPattern4;
			row4Enable		<= '0';

		when "110" =>
			nextRowPattern	<= rowPattern5;
			row5Enable		<= '0';

		when "111" =>
			nextRowPattern	<= rowPattern6;
			row6Enable 		<= '0';

		when others =>
			null;
	end case;
end process;

nextRowEnables <= row0Enable & row1Enable & row2Enable &
				  row3Enable & row4Enable & row5Enable &
				  row6Enable;

-- Driving the row clk count value into register
-- Doing the same with row enables and pattern
process(clk)
begin
	if rising_edge(clk) then
		if (nextRowEnable = '1') then
			rowPattern <= nextRowPattern;
			rowEnables <= nextRowEnables;
			rowCounter <= nextRowCounter;
		end if;
	end if;
end process;

end architecture rtl;
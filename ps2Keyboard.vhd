--------------------------------------------------------------------------------
-- Project		: PS2 Keyboard Driver
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 28/12/2021
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: ps2Keyboard.vhd
-- Design		: Interfacing a PS2 Keyboard with a Display Matrix on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_signed.all;

entity ps2Keyboard is
port (clk				: in std_logic;
	  ps2Clk			: in std_logic;
	  ps2Data			: in std_logic;
	  ps2NewCodeFlag	: out std_logic;
	  ps2Code 			: out std_logic_vector(7 downto 0));
end entity ps2Keyboard;

architecture rtl of ps2Keyboard is

-- Synchronising Flipflops
signal synchFlipflops 	: std_logic_vector(1 downto 0);

-- Debounced signals
signal ps2CLKDebounced	: std_logic;
signal ps2DataDebounced	: std_logic;

-- Shift Register
signal ps2DataWord		: std_logic_vector(10 downto 0);

-- Error Detection
signal isDataWordValid	: std_logic;

-- Used for counting time the keyboard was idle
signal idleCounter		: integer range 0 to 2499;

component debounce is
port (clk		: in std_logic;
	  button	: in std_logic;
	  output	: out std_logic);
end component debounce;

begin

-- Synchronising PS2 CLK and DATA Signals
process(clk)
begin
	if rising_edge(clk) then
		synchFlipflops(0) <= ps2Clk;
		synchFlipflops(1) <= ps2Data;
	end if;
end process;

-- Port Mapping
DebouncingPS2CLK : debounce
port map (clk 		=> clk,
		  button 	=> synchFlipflops(0),
		  output	=> ps2CLKDebounced);

DebouncingPS2Data : debounce
port map (clk 		=> clk,
		  button	=> synchFlipflops(1),
		  output	=> ps2DataDebounced);

-- Inputting Data Word in a Shift Register
process(ps2CLKDebounced)
begin
	if falling_edge(ps2CLKDebounced) then
		ps2DataWord <= ps2DataDebounced & ps2DataWord(10 downto 1);
	end if;
end process;

isDataWordValid <= not (not ps2DataWord(0) and ps2DataWord(10) and
				   (ps2DataWord(9) xor ps2DataWord(8) xor ps2DataWord(7) xor
				   ps2DataWord(6) xor ps2DataWord(5) xor ps2DataWord(4) xor
				   ps2DataWord(3) xor ps2DataWord(2) xor ps2DataWord(1)));

-- Output Process
process(clk)
begin
	if rising_edge(clk) then
		if (ps2CLKDebounced = '0') then
			idleCounter <= 0;
		elsif (idleCounter /= 2499) then
			idleCounter <= idleCounter + 1;
		end if;

		if (idleCounter = 2499 and isDataWordValid = '0') then
			ps2NewCodeFlag <= '1';
			ps2Code <= ps2DataWord(8 downto 1);
		else
			ps2NewCodeFlag <= '0';
		end if;
	end if;
end process;
	
end architecture rtl;
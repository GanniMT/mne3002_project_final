--------------------------------------------------------------------------------
-- Project		: Common Interface
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 21/01/2022
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: toplevel.vhd
-- Design		: Interfacing a PS2 Keyboard with a Matrix Display on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity toplevel is
port (clk				: in std_logic;
	  ps2Clk			: in std_logic;
	  ps2Data			: in std_logic;
	  charNew			: out std_logic;
	  matrixRow			: out std_logic_vector(6 downto 0);
	  matrixCol			: out std_logic_vector(4 downto 0);
	  anodeSel			: out std_logic_vector(3 downto 0);
	  sevenSegDispOut	: out std_logic_vector(6 downto 0));	
end entity toplevel;

architecture rtl of toplevel is

type states is (ready, newCode, translate, output);
signal state : states;

-- Signals related to Keyboard Codes
signal ps2CodeNew		: std_logic;
signal ps2Code 	  		: std_logic_vector(7 downto 0);
signal prevPS2CodeNew	: std_logic := '0';

-- Signals related to keystrokes
signal break          : std_logic := '0';	--'1' for break code, '0' for make code
signal e0_code        : std_logic := '0';	--'1' for multi-code commands, '0' for single code commands
signal caps_lock      : std_logic := '0';	--'1' if caps lock is active, '0' if caps lock is inactive
signal shift_r        : std_logic := '0';	--'1' if right shift is held down, else '0'
signal shift_l        : std_logic := '0';	--'1' if left shift is held down, else '0'

-- When FPGA is turned on, default pattern is displayed
signal matrixRow0	  : std_logic_vector(4 downto 0) := "10001";
signal matrixRow1	  : std_logic_vector(4 downto 0) := "01010";
signal matrixRow2	  : std_logic_vector(4 downto 0) := "00100";
signal matrixRow3	  : std_logic_vector(4 downto 0) := "01010";
signal matrixRow4	  : std_logic_vector(4 downto 0) := "00100";
signal matrixRow5	  : std_logic_vector(4 downto 0) := "01010";
signal matrixRow6	  : std_logic_vector(4 downto 0) := "10001";

-- Keyboard Driver Component Declaration
component ps2Keyboard is
port (clk				: in std_logic;
	  ps2Clk			: in std_logic;
	  ps2Data			: in std_logic;
	  ps2NewCodeFlag	: out std_logic;
	  ps2Code 			: out std_logic_vector(7 downto 0));
end component ps2Keyboard;

-- Seven Segment Display Driver Component Declaration
component sevenSegDisp is
port (clk				: in std_logic;
	  hexIn				: in std_logic_vector(7 downto 0);
	  anodeSel 			: out std_logic_vector(3 downto 0);
	  sevenSegDispOut	: out std_logic_vector(6 downto 0));
end component sevenSegDisp;

-- Dot Matrix Display Driver Component Decleration
component dotMatrix is
port (clk			: in std_logic;
	  rowPattern0	: in std_logic_vector(4 downto 0);
	  rowPattern1	: in std_logic_vector(4 downto 0);
	  rowPattern2	: in std_logic_vector(4 downto 0);
	  rowPattern3	: in std_logic_vector(4 downto 0);
	  rowPattern4	: in std_logic_vector(4 downto 0);
	  rowPattern5	: in std_logic_vector(4 downto 0);
	  rowPattern6	: in std_logic_vector(4 downto 0);
	  matrixRow		: out std_logic_vector(6 downto 0);
	  matrixCol		: out std_logic_vector(4 downto 0));
end component dotMatrix;

begin

ps2KeyboardMAP : ps2Keyboard
port map (clk				=> clk,
		  ps2Clk			=> ps2Clk,
		  ps2Data			=> ps2Data,
		  ps2NewCodeFlag	=> ps2CodeNew,
		  ps2Code 			=> ps2Code);

sevenSegDispMAP : sevenSegDisp
port map (clk				=> clk,
		  hexIn				=> ps2Code,
		  anodeSel 			=> anodeSel,
		  sevenSegDispOut	=> sevenSegDispOut);

dotMatrixMAP : dotMatrix
port map (clk			=> clk,
		  rowPattern0	=> matrixRow0,
		  rowPattern1	=> matrixRow1,
		  rowPattern2	=> matrixRow2,
		  rowPattern3	=> matrixRow3,
		  rowPattern4	=> matrixRow4,
		  rowPattern5	=> matrixRow5,
		  rowPattern6	=> matrixRow6,
		  matrixRow		=> matrixRow,
		  matrixCol		=> matrixCol);

-- Main Process
process(clk)
begin
	if rising_edge(clk) then
		prevPS2CodeNew <= ps2CodeNew;
		case state is

			-- Waiting for a new code to be received
			when ready =>
				if (prevPS2CodeNew = '0' and ps2CodeNew = '1') then
					charNew <= '0';
					state <= newCode;
				else
					state <= ready;
				end if;

			-- Determing what to do with new code
			when newCode =>
				if (ps2Code = x"F0") then
					break <= '1';
					state <= ready;
				elsif (ps2Code = x"E0") then
					e0_code <= '1';
					state <= ready;
				else
					state <= translate;
				end if;

			-- Translating code to output in dot matrix display
			when translate =>
				break <= '0';
				e0_code <= '0';

				case (ps2Code) is
					when x"58" =>
						if (break = '0') then
							caps_lock <= not caps_lock;
						end if;

					when x"12" =>
						shift_l <= not break;

					when x"59" =>
						shift_r <= not break;

					when others =>
						null;
				end case;

				-- Lowercase Letters and Numbers
				if ((shift_r = '0' and shift_l = '0' and caps_lock = '0') or
				((shift_r = '1' or shift_l = '1') and caps_lock = '1')) then

					case (ps2Code) is
						when x"1C" => 
						--a
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "01100";
						matrixRow3 <= "10010";
						matrixRow4 <= "10010";
						matrixRow5 <= "10010";
						matrixRow6 <= "01101";

						when x"32" =>
						--b
						matrixRow0 <= "00000";
						matrixRow1 <= "10000";
						matrixRow2 <= "10000";
						matrixRow3 <= "11100";
						matrixRow4 <= "10010";
						matrixRow5 <= "10010";
						matrixRow6 <= "11100";

						when x"21" =>
						--c
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "01100";
						matrixRow3 <= "10010";
						matrixRow4 <= "10000";
						matrixRow5 <= "10010";
						matrixRow6 <= "01100";

						when x"23" =>
						--d
						matrixRow0 <= "00000";
						matrixRow1 <= "00010";
						matrixRow2 <= "00010";
						matrixRow3 <= "01110";
						matrixRow4 <= "10010";
						matrixRow5 <= "10010";
						matrixRow6 <= "01110";

						when x"24" =>
						--e
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "01100";
						matrixRow3 <= "10010";
						matrixRow4 <= "11110";
						matrixRow5 <= "10000";
						matrixRow6 <= "01110";

						when x"2B" =>
						--f
						matrixRow0 <= "00000";
						matrixRow1 <= "00110";
						matrixRow2 <= "01000";
						matrixRow3 <= "01000";
						matrixRow4 <= "01110";
						matrixRow5 <= "01000";
						matrixRow6 <= "01000";

						when x"34" =>
						--g
						matrixRow0 <= "00000";
						matrixRow1 <= "01100";
						matrixRow2 <= "10010";
						matrixRow3 <= "10010";
						matrixRow4 <= "01110";
						matrixRow5 <= "00010";
						matrixRow6 <= "11100";

						when x"33" =>
						--h
						matrixRow0 <= "00000";
						matrixRow1 <= "10000";
						matrixRow2 <= "10000";
						matrixRow3 <= "11100";
						matrixRow4 <= "10010";
						matrixRow5 <= "10010";
						matrixRow6 <= "10010";

						when x"43" =>
						--i
						matrixRow0 <= "00000";
						matrixRow1 <= "00100";
						matrixRow2 <= "00000";
						matrixRow3 <= "00100";
						matrixRow4 <= "00100";
						matrixRow5 <= "00100";
						matrixRow6 <= "00100";

						when x"3B" =>
						--j
						matrixRow0 <= "00010";
						matrixRow1 <= "00000";
						matrixRow2 <= "00010";
						matrixRow3 <= "00010";
						matrixRow4 <= "00010";
						matrixRow5 <= "10010";
						matrixRow6 <= "01100";

						when x"42" =>
						--k
						matrixRow0 <= "00000";
						matrixRow1 <= "10000";
						matrixRow2 <= "10010";
						matrixRow3 <= "10100";
						matrixRow4 <= "11000";
						matrixRow5 <= "10100";
						matrixRow6 <= "10010";

						when x"4B" =>
						--l
						matrixRow0 <= "00000";
						matrixRow1 <= "00100";
						matrixRow2 <= "00100";
						matrixRow3 <= "00100";
						matrixRow4 <= "00100";
						matrixRow5 <= "00100";
						matrixRow6 <= "00100";
						when x"3A" =>
						--m
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "01010";
						matrixRow3 <= "10101";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "10001";

						when x"31" =>
						--n
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "10100";
						matrixRow3 <= "11010";
						matrixRow4 <= "10010";
						matrixRow5 <= "10010";
						matrixRow6 <= "10010";

						when x"44" =>
						--o
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "01100";
						matrixRow3 <= "10010";
						matrixRow4 <= "10010";
						matrixRow5 <= "10010";
						matrixRow6 <= "01100";

						when x"4D" =>
						--p
						matrixRow0 <= "00000";
						matrixRow1 <= "11100";
						matrixRow2 <= "10010";
						matrixRow3 <= "10010";
						matrixRow4 <= "11100";
						matrixRow5 <= "10000";
						matrixRow6 <= "10000";

						when x"15" =>
						--q
						matrixRow0 <= "00000";
						matrixRow1 <= "01110";
						matrixRow2 <= "10010";
						matrixRow3 <= "10010";
						matrixRow4 <= "01110";
						matrixRow5 <= "00010";
						matrixRow6 <= "00010";

						when x"2D" =>
						--r
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "10110";
						matrixRow3 <= "11000";
						matrixRow4 <= "10000";
						matrixRow5 <= "10000";
						matrixRow6 <= "10000";

						when x"1B" =>
						--s
						matrixRow0 <= "00000";
						matrixRow1 <= "01100";
						matrixRow2 <= "10010";
						matrixRow3 <= "01000";
						matrixRow4 <= "00100";
						matrixRow5 <= "10010";
						matrixRow6 <= "01100";

						when x"2C" =>
						--t
						matrixRow0 <= "00000";
						matrixRow1 <= "01000";
						matrixRow2 <= "01000";
						matrixRow3 <= "01110";
						matrixRow4 <= "01000";
						matrixRow5 <= "01000";
						matrixRow6 <= "00110";

						when x"3C" =>
						--u
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "10010";
						matrixRow3 <= "10010";
						matrixRow4 <= "10010";
						matrixRow5 <= "10010";
						matrixRow6 <= "01100";

						when x"2A" =>
						--v
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "10001";
						matrixRow3 <= "10001";
						matrixRow4 <= "10001";
						matrixRow5 <= "01010";
						matrixRow6 <= "00100";

						when x"1D" =>
						--w
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "10001";
						matrixRow3 <= "10001";
						matrixRow4 <= "10001";
						matrixRow5 <= "10101";
						matrixRow6 <= "01010";

						when x"22" =>
						--x
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "10001";
						matrixRow3 <= "01010";
						matrixRow4 <= "00100";
						matrixRow5 <= "01010";
						matrixRow6 <= "10001";

						when x"35" =>
						--y
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "10001";
						matrixRow3 <= "01010";
						matrixRow4 <= "00100";
						matrixRow5 <= "00100";
						matrixRow6 <= "00100";

						when x"1A" =>
						--z
						matrixRow0 <= "00000";
						matrixRow1 <= "00000";
						matrixRow2 <= "11111";
						matrixRow3 <= "00010";
						matrixRow4 <= "00100";
						matrixRow5 <= "01000";
						matrixRow6 <= "11111";

						when x"45" =>
						--0
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "10001";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"16" =>
						--1
						matrixRow0 <= "00100";
						matrixRow1 <= "01100";
						matrixRow2 <= "10100";
						matrixRow3 <= "00100";
						matrixRow4 <= "00100";
						matrixRow5 <= "00100";
						matrixRow6 <= "11111";

						when x"1E" =>
						--2
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "00001";
						matrixRow3 <= "00010";
						matrixRow4 <= "00100";
						matrixRow5 <= "01000";
						matrixRow6 <= "11111";

						when x"26" =>
						--3
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "00001";
						matrixRow3 <= "00010";
						matrixRow4 <= "00001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"25" =>
						--4
						matrixRow0 <= "10001";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "11111";
						matrixRow4 <= "00001";
						matrixRow5 <= "00001";
						matrixRow6 <= "00001";

						when x"2E" =>
						--5
						matrixRow0 <= "11111";
						matrixRow1 <= "10000";
						matrixRow2 <= "10000";
						matrixRow3 <= "11110";
						matrixRow4 <= "00001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01111";

						when x"36" =>
						--6
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10000";
						matrixRow3 <= "11110";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"3D" =>
						--7
						matrixRow0 <= "11111";
						matrixRow1 <= "00001";
						matrixRow2 <= "00010";
						matrixRow3 <= "00100";
						matrixRow4 <= "00100";
						matrixRow5 <= "00100";
						matrixRow6 <= "00100";

						when x"3E" =>
						--8
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "01110";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"46" =>
						--9
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "01111";
						matrixRow4 <= "00001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when others =>
							null;
					end case;

				elsif ((shift_r = '1' and shift_l = '1' and caps_lock = '1') or
				((shift_r = '0' or shift_l = '0') and caps_lock = '0')) then

					case (ps2Code) is
						when x"1C" =>
						--A
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "11111";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "10001";

						when x"32" =>
						--B
						matrixRow0 <= "11110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "11110";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "11110";

						when x"21" =>
						--C
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "10000";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"23" =>
						--D
						matrixRow0 <= "11110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "10001";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "11110";

						when x"24" =>
						--E
						matrixRow0 <= "11111";
						matrixRow1 <= "10000";
						matrixRow2 <= "10000";
						matrixRow3 <= "11110";
						matrixRow4 <= "10000";
						matrixRow5 <= "10000";
						matrixRow6 <= "11111";

						when x"2B" =>
						--F
						matrixRow0 <= "11111";
						matrixRow1 <= "10000";
						matrixRow2 <= "10000";
						matrixRow3 <= "11110";
						matrixRow4 <= "10000";
						matrixRow5 <= "10000";
						matrixRow6 <= "10000";

						when x"34" =>
						--G
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "10000";
						matrixRow4 <= "10111";
						matrixRow5 <= "10001";
						matrixRow6 <= "01111";

						when x"33" =>
						--H
						matrixRow0 <= "10001";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "11111";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "10001";

						when x"43" =>
						--I
						matrixRow0 <= "11111";
						matrixRow1 <= "00100";
						matrixRow2 <= "00100";
						matrixRow3 <= "00100";
						matrixRow4 <= "00100";
						matrixRow5 <= "00100";
						matrixRow6 <= "11111";

						when x"3B" =>
						--J
						matrixRow0 <= "11111";
						matrixRow1 <= "00001";
						matrixRow2 <= "00001";
						matrixRow3 <= "00001";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"42" =>
						--K
						matrixRow0 <= "10001";
						matrixRow1 <= "10010";
						matrixRow2 <= "10100";
						matrixRow3 <= "11000";
						matrixRow4 <= "10100";
						matrixRow5 <= "10010";
						matrixRow6 <= "10001";

						when x"4B" =>
						--L
						matrixRow0 <= "10000";
						matrixRow1 <= "10000";
						matrixRow2 <= "10000";
						matrixRow3 <= "10000";
						matrixRow4 <= "10000";
						matrixRow5 <= "10000";
						matrixRow6 <= "11111";

						when x"3A" =>
						--M
						matrixRow0 <= "10001";
						matrixRow1 <= "11011";
						matrixRow2 <= "10101";
						matrixRow3 <= "10001";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "10001";

						when x"31" =>
						--N
						matrixRow0 <= "10001";
						matrixRow1 <= "10001";
						matrixRow2 <= "11001";
						matrixRow3 <= "10101";
						matrixRow4 <= "10011";
						matrixRow5 <= "10001";
						matrixRow6 <= "10001";

						when x"44" =>
						--O
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "10001";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"4D" =>
						--P
						matrixRow0 <= "11110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "11110";
						matrixRow4 <= "10000";
						matrixRow5 <= "10000";
						matrixRow6 <= "10000";

						when x"15" =>
						--Q
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "10001";
						matrixRow4 <= "10101";
						matrixRow5 <= "01110";
						matrixRow6 <= "00001";

						when x"2D" =>
						--R
						matrixRow0 <= "11110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "11110";
						matrixRow4 <= "10100";
						matrixRow5 <= "10010";
						matrixRow6 <= "10001";

						when x"1B" =>
						--S
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "01001";
						matrixRow3 <= "00100";
						matrixRow4 <= "10010";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"2C" =>
						--T
						matrixRow0 <= "11111";
						matrixRow1 <= "00100";
						matrixRow2 <= "00100";
						matrixRow3 <= "00100";
						matrixRow4 <= "00100";
						matrixRow5 <= "00100";
						matrixRow6 <= "00100";


						when x"3C" =>
						--U
						matrixRow0 <= "10001";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "10001";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"2A" =>
						--V
						matrixRow0 <= "10001";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "10001";
						matrixRow4 <= "10001";
						matrixRow5 <= "01010";
						matrixRow6 <= "00100";

						when x"1D" =>
						--W
						matrixRow0 <= "10001";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "10001";
						matrixRow4 <= "10101";
						matrixRow5 <= "11011";
						matrixRow6 <= "10001";

						when x"22" =>
						--X
						matrixRow0 <= "10001";
						matrixRow1 <= "10001";
						matrixRow2 <= "01010";
						matrixRow3 <= "00100";
						matrixRow4 <= "01010";
						matrixRow5 <= "10001";
						matrixRow6 <= "10001";

						when x"35" =>
						--Y
						matrixRow0 <= "10001";
						matrixRow1 <= "10001";
						matrixRow2 <= "01010";
						matrixRow3 <= "00100";
						matrixRow4 <= "00100";
						matrixRow5 <= "00100";
						matrixRow6 <= "00100";

						when x"1A" =>
						--Z
						matrixRow0 <= "11111";
						matrixRow1 <= "00001";
						matrixRow2 <= "00010";
						matrixRow3 <= "00100";
						matrixRow4 <= "01000";
						matrixRow5 <= "10000";
						matrixRow6 <= "11111";

						when x"45" =>
						--0
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "10001";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"16" =>
						--1
						matrixRow0 <= "00100";
						matrixRow1 <= "01100";
						matrixRow2 <= "10100";
						matrixRow3 <= "00100";
						matrixRow4 <= "00100";
						matrixRow5 <= "00100";
						matrixRow6 <= "11111";

						when x"1E" =>
						--2
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "00001";
						matrixRow3 <= "00010";
						matrixRow4 <= "00100";
						matrixRow5 <= "01000";
						matrixRow6 <= "11111";

						when x"26" =>
						--3
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "00001";
						matrixRow3 <= "00010";
						matrixRow4 <= "00001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"25" =>
						--4
						matrixRow0 <= "10001";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "11111";
						matrixRow4 <= "00001";
						matrixRow5 <= "00001";
						matrixRow6 <= "00001";

						when x"2E" =>
						--5
						matrixRow0 <= "11111";
						matrixRow1 <= "10000";
						matrixRow2 <= "10000";
						matrixRow3 <= "11110";
						matrixRow4 <= "00001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01111";

						when x"36" =>
						--6
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10000";
						matrixRow3 <= "11110";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"3D" =>
						--7
						matrixRow0 <= "11111";
						matrixRow1 <= "00001";
						matrixRow2 <= "00010";
						matrixRow3 <= "00100";
						matrixRow4 <= "00100";
						matrixRow5 <= "00100";
						matrixRow6 <= "00100";

						when x"3E" =>
						--8
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "01110";
						matrixRow4 <= "10001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when x"46" =>
						--9
						matrixRow0 <= "01110";
						matrixRow1 <= "10001";
						matrixRow2 <= "10001";
						matrixRow3 <= "01111";
						matrixRow4 <= "00001";
						matrixRow5 <= "10001";
						matrixRow6 <= "01110";

						when others =>
							null;

					end case;
				end if;

				if (break = '0') then
					state <= output;
				else
					state <= ready;
				end if;

			-- Output flag is out
			when output =>
				charNew <= '1';
				if (prevPS2CodeNew = '0' and ps2CodeNew = '1') then
					state <= newCode;
				else
					state <= output;
				end if;
		end case;
	end if;
end process;
end architecture rtl;
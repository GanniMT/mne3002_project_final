--------------------------------------------------------------------------------
-- Project		: PS2 Keyboard Driver (Testbench)
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 28/12/2021
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: ps2Keyboard_TB.vhd
-- Design		: Interfacing a PS2 Keyboard with a Display Matrix on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity ps2Keyboard_TB is
port (clk		: out std_logic;
	  ps2Clk	: out std_logic;
	  ps2Data	: out std_logic);
end entity ps2Keyboard_TB;

architecture sim of ps2Keyboard_TB is

constant fpgaClockPeriod : time := 20 ns;
constant ps2clockPeriod : time := 100 us; --10kHz clock

begin

-- Clock process definitions
ps2ClkProcess : process
begin
ps2Clk <= '0';
wait for ps2clockPeriod/2;
ps2Clk <= '1';
wait for ps2clockPeriod/2;
end process ps2ClkProcess;

fpgaClkProcess : process
begin
clk <= '0';
wait for fpgaClockPeriod/2;
clk <= '1';
wait for fpgaClockPeriod/2;
end process fpgaClkProcess;

-- Stimulus Process
--pressing W on keyboard
dataInProcess : process
begin
wait for ps2clockPeriod;
ps2Data <= '0'; --start bit
wait for ps2clockPeriod;
ps2Data <= '1'; --LSB
wait for ps2clockPeriod;
ps2Data <= '0';
wait for ps2clockPeriod;
ps2Data <= '1';
wait for ps2clockPeriod;
ps2Data <= '1';
wait for ps2clockPeriod;
ps2Data <= '1';
wait for ps2clockPeriod;
ps2Data <= '0';
wait for ps2clockPeriod;
ps2Data <= '0';
wait for ps2clockPeriod;
ps2Data <= '0';--MSB
wait for ps2clockPeriod;
ps2Data <= '1';--Parity
wait for ps2clockPeriod;
ps2Data <= '1';--Stop Bit
wait for ps2clockPeriod;
ps2Data <= '0';
wait for ps2clockPeriod;
end process dataInProcess;
	
end architecture sim;
--------------------------------------------------------------------------------
-- Project		: PS2 Keyboard Driver (Toplevel)
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 28/12/2021
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: ps2Keyboard_TL.vhd
-- Design		: Interfacing a PS2 Keyboard with a Display Matrix on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity ps2Keyboard_TL is
port (ps2NewCodeFlag	: out std_logic;
	  ps2Code			: out std_logic_vector);	
end entity ps2Keyboard_TL;

architecture TL_arch of ps2Keyboard_TL is

component ps2Keyboard is
port (clk				: in std_logic;
	  ps2Clk			: in std_logic;
	  ps2Data			: in std_logic;
	  ps2NewCodeFlag	: out std_logic;
	  ps2Code 			: out std_logic_vector(7 downto 0));
end component ps2Keyboard;

component ps2Keyboard_TB is
port (clk		: out std_logic;
	  ps2Clk	: out std_logic;
	  ps2Data	: out std_logic);
end component ps2Keyboard_TB;

signal clk	 	: std_logic;
signal ps2Clk	: std_logic;
signal ps2Data	: std_logic;

begin

ps2Keyboard_MAP : ps2Keyboard
port map (clk				=> clk,
	  	  ps2Clk			=> ps2Clk,
	  	  ps2Data			=> ps2Data,
	  	  ps2NewCodeFlag	=> ps2NewCodeFlag,
	  	  ps2Code 			=> ps2Code);

ps2Keyboard_TB_MAP : ps2Keyboard_TB
port map (clk		=> clk,
	  	  ps2Clk	=> ps2Clk,
	  	  ps2Data	=> ps2Data);
	
end architecture TL_arch;
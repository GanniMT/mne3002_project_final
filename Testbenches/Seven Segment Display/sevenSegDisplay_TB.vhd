--------------------------------------------------------------------------------
-- Project		: Seven Segment Display Driver (Testbench)
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 04/01/2022
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: sevenSegDisplay_TB.vhd
-- Design		: Interfacing a PS2 Keyboard with a Matrix Display on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity sevenSegDisp_TB is
port (clk	: out std_logic;
	  hexIn	: out std_logic_vector(7 downto 0));	
end entity sevenSegDisp_TB;

architecture sim of sevenSegDisp_TB is

constant fpgaClockPeriod : time := 20 ns;

begin

fpgaClkProcess : process
begin
clk <= '0';
wait for fpgaClockPeriod/2;
clk <= '1';
wait for fpgaClockPeriod/2;
end process fpgaClkProcess;

-- Stimulus Process
hexInProcess : process
begin
hexIn <= x"1D";
wait for 10*fpgaClockPeriod;

end process hexInProcess;
	
end architecture sim;
--------------------------------------------------------------------------------
-- Project		: Seven Segment Display Driver (Toplevel)
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 04/01/2022
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: sevenSegDisplay_TL.vhd
-- Design		: Interfacing a PS2 Keyboard with a Matrix Display on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity sevenSegDisp_TL is
port (anodeSel			: out std_logic_vector(3 downto 0);
	  sevenSegDispOut	: out std_logic_vector(6 downto 0));
end entity sevenSegDisp_TL;

architecture TL_arch of sevenSegDisp_TL is

component sevenSegDisp is
port (clk				: in std_logic;
	  hexIn				: in std_logic_vector(7 downto 0);
	  anodeSel 			: out std_logic_vector(3 downto 0);
	  sevenSegDispOut	: out std_logic_vector(6 downto 0));
end component sevenSegDisp;

component sevenSegDisp_TB is
port (clk	: out std_logic;
	  hexIn	: out std_logic_vector (7 downto 0));	
end component sevenSegDisp_TB;

signal clk		: std_logic;
signal hexIn	: std_logic_vector(7 downto 0);

begin

sevenSegDisp_MAP : sevenSegDisp
port map (clk				=> clk,
	  	  hexIn				=> hexIn,
	  	  anodeSel 			=> anodeSel,
	      sevenSegDispOut	=> sevenSegDispOut);

sevenSegDisp_TB_MAP : sevenSegDisp_TB
port map (clk	=> clk,
	  	  hexIn	=> hexIn);
	
end architecture TL_arch;
--------------------------------------------------------------------------------
-- Project		: Debounce Logic (Testbench)
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 28/12/2021
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: debounce_TB.vhd
-- Design		: Interfacing a PS2 Keyboard with a Matrix Display on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity debounce_TB is
port (clk		: out std_logic;
	  button	: out std_logic);
end entity debounce_TB;

architecture sim of debounce_TB is

constant fpgaClockPeriod : time := 20 ns;

begin

-- Clock process definition
fpgaClkProcess : process
begin
clk <= '0';
wait for fpgaClockPeriod/2;
clk <= '1';
wait for fpgaClockPeriod/2;
end process fpgaClkProcess;

buttonProcess : process
begin
button <= '0';
wait for 30 ns;
button <= '1';
wait for 30 ns;
button <= '0';
wait for 40 ns; 
button <= '1';
wait for 1000 ns;  
button <= '0';
wait for 10 ns; 
button <= '1';
wait for 30 ns; 
button <= '0';
end process buttonProcess;
	
end architecture sim;
--------------------------------------------------------------------------------
-- Project		: Debounce Logic (Toplevel)
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 28/12/2021
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: debounce_TL.vhd
-- Design		: Interfacing a PS2 Keyboard with a Matrix Display on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity debounce_TL is
port (output	: out std_logic);
end entity debounce_TL;

architecture TL_arch of debounce_TL is

component debounce is
port (clk		: in std_logic;
	  button	: in std_logic;
	  output	: out std_logic);
end component debounce;

component debounce_TB is
port (clk		: out std_logic;
	  button	: out std_logic);
end component debounce_TB;

signal clk		: std_logic;
signal button	: std_logic;

begin

debounce_MAP : debounce
port map (clk		=> clk,
		  button	=> button,
		  output	=> output);

debounce_TB_MAP : debounce_TB
port map (clk		=> clk,
	  	  button	=> button);
	
end architecture TL_arch;
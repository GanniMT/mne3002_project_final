--------------------------------------------------------------------------------
-- Project		: Common Interface (Toplevel)
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 21/01/2022
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: toplevel_TL.vhd
-- Design		: Interfacing a PS2 Keyboard with a Matrix Display on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity toplevel_TL is
port (charNew			: out std_logic;
	  matrixRow			: out std_logic_vector(6 downto 0);
	  matrixCol			: out std_logic_vector(4 downto 0);
	  anodeSel			: out std_logic_vector(3 downto 0);
	  sevenSegDispOut	: out std_logic_vector(6 downto 0));
end entity toplevel_TL;

architecture TL_arch of toplevel_TL is

component toplevel is
port (clk				: in std_logic;
	  ps2Clk			: in std_logic;
	  ps2Data			: in std_logic;
	  charNew			: out std_logic;
	  matrixRow			: out std_logic_vector(6 downto 0);
	  matrixCol			: out std_logic_vector(4 downto 0);
	  anodeSel			: out std_logic_vector(3 downto 0);
	  sevenSegDispOut	: out std_logic_vector(6 downto 0));	
end component toplevel;

component ps2Keyboard_TB is
port (clk		: out std_logic;
	  ps2Clk	: out std_logic;
	  ps2Data	: out std_logic);
end component ps2Keyboard_TB;

signal clk		: std_logic;
signal ps2Clk	: std_logic;
signal ps2Data	: std_logic;

begin

toplevel_MAP : toplevel
port map (clk				=> clk,
	  	  ps2Clk			=> ps2Clk,
	  	  ps2Data			=> ps2Data,
	  	  charNew			=> charNew,
	  	  matrixRow			=> matrixRow,
	  	  matrixCol			=> matrixCol,
	  	  anodeSel			=> anodeSel,
	  	  sevenSegDispOut	=> sevenSegDispOut);

ps2Keyboard_TB_MAP : ps2Keyboard_TB
port map (clk		=> clk,
	  	  ps2Clk	=> ps2Clk,
	  	  ps2Data	=> ps2Data);
	
end architecture TL_arch;
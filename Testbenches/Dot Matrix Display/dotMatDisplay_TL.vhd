--------------------------------------------------------------------------------
-- Project		: Dot Matrix Display Driver (Toplevel)
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 05/02/2022
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: dotMatDisplay_TL.vhd
-- Design		: Interfacing a PS2 Keyboard with a Matrix Display on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity dotMatrix_TL is
port (matrixRow		: out std_logic_vector(6 downto 0);
	  matrixCol		: out std_logic_vector(4 downto 0));
end entity dotMatrix_TL;

architecture TL_arch of dotMatrix_TL is

component dotMatrix is
port (clk			: in std_logic;
	  rowPattern0	: in std_logic_vector(4 downto 0);
	  rowPattern1	: in std_logic_vector(4 downto 0);
	  rowPattern2	: in std_logic_vector(4 downto 0);
	  rowPattern3	: in std_logic_vector(4 downto 0);
	  rowPattern4	: in std_logic_vector(4 downto 0);
	  rowPattern5	: in std_logic_vector(4 downto 0);
	  rowPattern6	: in std_logic_vector(4 downto 0);
	  matrixRow		: out std_logic_vector(6 downto 0);
	  matrixCol		: out std_logic_vector(4 downto 0));
end component dotMatrix;

component dotMatrix_TB is
port (clk			: out std_logic;
	  rowPattern0	: out std_logic_vector(4 downto 0);
	  rowPattern1	: out std_logic_vector(4 downto 0);
	  rowPattern2	: out std_logic_vector(4 downto 0);
	  rowPattern3	: out std_logic_vector(4 downto 0);
	  rowPattern4	: out std_logic_vector(4 downto 0);
	  rowPattern5	: out std_logic_vector(4 downto 0);
	  rowPattern6	: out std_logic_vector(4 downto 0));
end component dotMatrix_TB;

signal clk			: std_logic;
signal rowPattern0	: std_logic_vector(4 downto 0);
signal rowPattern1	: std_logic_vector(4 downto 0);
signal rowPattern2	: std_logic_vector(4 downto 0);
signal rowPattern3	: std_logic_vector(4 downto 0);
signal rowPattern4	: std_logic_vector(4 downto 0);
signal rowPattern5	: std_logic_vector(4 downto 0);
signal rowPattern6	: std_logic_vector(4 downto 0);

begin

dotMatrix_MAP : dotMatrix
port map (clk			=> clk,
	  	  rowPattern0	=> rowPattern0,
	  	  rowPattern1	=> rowPattern1,
	  	  rowPattern2	=> rowPattern2,
	  	  rowPattern3	=> rowPattern3,
	  	  rowPattern4	=> rowPattern4,
	  	  rowPattern5	=> rowPattern5,
	  	  rowPattern6	=> rowPattern6,
	  	  matrixRow		=> matrixRow,
	  	  matrixCol		=> matrixCol);

dotMatrix_TB_MAP : dotMatrix_TB
port map (clk			=> clk,
	  	  rowPattern0	=> rowPattern0,
	  	  rowPattern1	=> rowPattern1,
	  	  rowPattern2	=> rowPattern2,
	  	  rowPattern3	=> rowPattern3,
	  	  rowPattern4	=> rowPattern4,
	  	  rowPattern5	=> rowPattern5,
	  	  rowPattern6	=> rowPattern6);

end architecture TL_arch;
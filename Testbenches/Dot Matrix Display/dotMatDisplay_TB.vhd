--------------------------------------------------------------------------------
-- Project		: Dot Matrix Display Driver (Testbench)
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 05/02/2022
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: dotMatDisplay_TB.vhd
-- Design		: Interfacing a PS2 Keyboard with a Matrix Display on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity dotMatrix_TB is
port (clk			: out std_logic;
	  rowPattern0	: out std_logic_vector(4 downto 0);
	  rowPattern1	: out std_logic_vector(4 downto 0);
	  rowPattern2	: out std_logic_vector(4 downto 0);
	  rowPattern3	: out std_logic_vector(4 downto 0);
	  rowPattern4	: out std_logic_vector(4 downto 0);
	  rowPattern5	: out std_logic_vector(4 downto 0);
	  rowPattern6	: out std_logic_vector(4 downto 0));
end entity dotMatrix_TB;

architecture sim of dotMatrix_TB is

constant fpgaClockPeriod : time := 20 ns;

begin

-- Clock process definitions
fpgaClkProcess : process
begin
clk <= '0';
wait for fpgaClockPeriod/2;
clk <= '1';
wait for fpgaClockPeriod/2;
end process fpgaClkProcess;

--Stimulus Process
rowPatternsProcess : process
begin
rowPattern0 <= "10001";
rowPattern1 <= "01010";
rowPattern2 <= "00100";
rowPattern3 <= "01010";
rowPattern4 <= "00100";
rowPattern5 <= "01010";
rowPattern6 <= "10001";
wait;
end process rowPatternsProcess;
	
end architecture sim;
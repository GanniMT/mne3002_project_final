--------------------------------------------------------------------------------
-- Project		: Debounce Logic
-- Authors		: Jonathan Camenzuli and Gabe Vassallo
-- Dates/s		: 28/12/2021
-- Department	: Department of MNE, Faculty of ICT, University of Malta
--
-- File			: debounce.vhd
-- Design		: Interfacing a PS2 Keyboard with a Matrix Display on a FPGA
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity debounce is
port (clk		: in std_logic;
	  button	: in std_logic;
	  output	: out std_logic := '0');
end entity debounce;

architecture rtl of debounce is

begin

-- Main Process
process(clk)

-- 8-bit Shift Register
variable reg	: std_logic_vector(7 downto 0) := (others => '0');

-- Acts as the MSB of the Shift Register
variable regMSB	: std_logic;

begin
	if rising_edge(clk) then
		regMSB := button;
		reg(6 downto 0) := reg(7 downto 1);
		reg(7) := regMSB;

		if (reg = "00000000") then
			output <= '0';
		elsif (reg = "11111111") then
			output <= '1';
		end if;
	end if;
end process;
	
end architecture rtl;